﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : AutoCreateInstance<ObjectPool> {
    
    public ObjectCache[] caches;

    Hashtable activeCachedObjects;

    void Start()
    {
        int amount = 0;
        for (int i = 0; i < caches.Length; i++)
        {
            caches[i].Initialize();
            amount += caches[i].cacheSize;
        }
        
        activeCachedObjects = new Hashtable(amount);
    }

    public GameObject Spawn(GameObject prefab, Vector3 position, Quaternion rotation)
    {
        ObjectCache cache = null;
        
        for (int i = 0; i < caches.Length; i++)
        {
            if (caches[i].prefab == prefab)
            {
                cache = caches[i];
            }
        }
        
        if (cache == null)
        {
            return Instantiate(prefab, position, rotation) as GameObject;
        }
        
        GameObject obj = cache.GetNextObjectInCache();
        
        obj.transform.position = position;
        obj.transform.rotation = rotation;
        
        obj.SetActive(true);
        activeCachedObjects[obj] = true;
        return obj;
    }

    public void Destroy(GameObject goToDestroy)
    {
        if (activeCachedObjects.ContainsKey(goToDestroy))
        {
            goToDestroy.SetActive(false);
            activeCachedObjects[goToDestroy] = false;
        }
        else
        {
            Destroy(goToDestroy);
        }
    }
}

[Serializable]
public class ObjectCache
{
    public GameObject prefab;
    public int cacheSize = 30;

    private GameObject[] objects;
    private int cacheIndex = 0;

    public void Initialize()
    {
        objects = new GameObject[cacheSize];
        
        for (int i = 0; i < cacheSize; i++)
        {
            objects[i] = GameObject.Instantiate(prefab) as GameObject;
            objects[i].SetActive(false);
            objects[i].name = objects[i].name + i;
        }
    }

    public GameObject GetNextObjectInCache()
    {
        GameObject obj = null;
        for (int i = 0; i < cacheSize; i++)
        {
            obj = objects[cacheIndex];
            if (!obj.activeSelf)
                break;
            cacheIndex = (cacheIndex + 1) % cacheSize;
        }
        
        if (obj.activeSelf)
        {
            ObjectPool.Instance.Destroy(obj);
        }
        cacheIndex = (cacheIndex + 1) % cacheSize;

        return obj;
    }

}
