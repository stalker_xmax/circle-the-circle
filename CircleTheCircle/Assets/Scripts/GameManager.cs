﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum GameState
{
    Idle,
    Play,
    Pause,
    Over
}

[Serializable]
public class LayerParallax
{
    public Transform ParallaxTran;
    public float ParallaxDepth;
}

public class GameManager : AutoCreateInstance<GameManager> {

    public const int FIX_POINT = 10;

    public GameState State = GameState.Idle;
    public float GameSpeed = 5;
    public float ReloopPosition = -12.8f;
    
    [SerializeField]
    private LayerParallax[] _layerParallaxs;

    [SerializeField]
    private Character _character;

    [SerializeField]
    private ObstacleSpawner _obstacleSpawner;

    [SerializeField]
    private PostLeaderBoard _postLeaderBoard;

    private int _score;
    public int Score
    {
        get { return _score; }
        set
        {
            _score = value;
            UIManager.Instance.UpdateScore(_score);
        }
    }

    void Update()
    {

        if (State != GameState.Over)
        {
            for (int i = 0, l = _layerParallaxs.Length; i < l; i++)
            {
                var layer = _layerParallaxs[i];
                if (layer != null)
                {
                    var tran = layer.ParallaxTran;
                    tran.Translate(Vector3.left * Mathf.Ceil(GameSpeed * layer.ParallaxDepth) * Time.smoothDeltaTime, Space.Self);
                    if (tran.position.x < ReloopPosition)
                    {
                        tran.position = new Vector3(0, tran.position.y, 0);
                    }
                }
            }
        }
    }

    public void StartGame()
    {
        _character.Play();
        Score = 0;
        _obstacleSpawner.StartSpawn();
        State = GameState.Play;
    }

    public void GameOver()
    {
        _obstacleSpawner.StopSpawn();
        State = GameState.Over;
        UIManager.Instance.ChangeState(UIState.GameOver);
        _postLeaderBoard.Post(Score);
    }

    public void Replay()
    {
        ClearObstacles();
    }

    private void ClearObstacles()
    {
        GameObject[] obstacles = GameObject.FindGameObjectsWithTag(Character.OBSTACLE_TAG);

        foreach (GameObject go in obstacles)
        {
            ObjectPool.Instance.Destroy(go);
        }
    }
}
