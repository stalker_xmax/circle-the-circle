﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CharacterState
{
    Run,
    Jump,
    Death
}

public class Character : MonoBehaviour
{
    public const string ANIMATOR_STATE_TRIGGER = "CharacterState";
    public const string OBSTACLE_TAG = "Obstacle";
    public const string SCORE_TAG = "Score";

    public CharacterState State = CharacterState.Run;
    [SerializeField]
    private float _jumpForce = 80;
    [SerializeField]
    private CircleCollider2D _circleCollider;
    [SerializeField]
    private Transform _pointCheckGround;
    [SerializeField]
    private Rigidbody2D _rigidbody2D;
    [SerializeField]
    private Animator _animator;
    [SerializeField]
    private AudioSource _audioSource;
    [SerializeField]
    private AudioClip _clipLose;

    private bool _isOnGround = true;    
    private bool IsOnGround
    {
        get { return _isOnGround; }
        set
        {
            _animator.SetBool("OnGround", value);
            _isOnGround = value;
            if (value)
            {
                ChangeState(CharacterState.Run);
            }
        }
    }

    public void Play()
    {
        ChangeState(CharacterState.Run);
    }

    void Update()
    {
        if (State != CharacterState.Death)
        {
            IsOnGround = Physics2D.Linecast(transform.position, _pointCheckGround.position, 1 << LayerMask.NameToLayer("Ground"));
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (_isOnGround && State == CharacterState.Run && 
                GameManager.Instance.State == GameState.Play &&
                UIManager.Instance.State == UIState.Play)
            {
                _rigidbody2D.AddForce(Vector2.up * _jumpForce * GameManager.Instance.GameSpeed);
                ChangeState(CharacterState.Jump);
            }
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == OBSTACLE_TAG)
        {
            ChangeState(CharacterState.Death);
            GameManager.Instance.GameOver();
            _audioSource.PlayOneShot(_clipLose);
        }
        else if (col.gameObject.tag == SCORE_TAG)
        {
            GameManager.Instance.Score += GameManager.FIX_POINT;
            Debug.Log("get score! :" + GameManager.Instance.Score);
        }
    }

    public void Replay()
    {
        ChangeState(CharacterState.Run);
    }

    private void ChangeState(CharacterState state)
    {
        State = state;
        _animator.SetInteger(ANIMATOR_STATE_TRIGGER, (int)state);
    }
}
