﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject _obstaclePrefab;
    public float durationSpawn = 10f;

    public void StartSpawn()
    {
        StartCoroutine(SpawnObstacle());
    }

    public void StopSpawn()
    {
       StopAllCoroutines();
    }

    private IEnumerator SpawnObstacle()
    {
        ObjectPool.Instance.Spawn(_obstaclePrefab, transform.position, new Quaternion());
        yield return new WaitForSeconds((durationSpawn + (Random.Range(0, 11) - 3)) / GameManager.Instance.GameSpeed);
        StartCoroutine(SpawnObstacle());
    }
}
