﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[Serializable]
public class UserScore
{
    public string userName;
    public int score;
}

public class PostLeaderBoard : MonoBehaviour {

    public void Post(int score)
    {
        var myScore = new UserScore
        {
            score = score,
            userName = "ngongoctri"
        };
        var json = JsonUtility.ToJson(myScore);
        Debug.Log("json: " + json);
        StartCoroutine(Upload(json));
    }

    private IEnumerator Upload(string json)
    {
        List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
        formData.Add(new MultipartFormDataSection(json));

        UnityWebRequest www = UnityWebRequest.Post("http://www.kixeye.com/leaderboard", formData);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Post leaderboard complete!");
        }
    }
}
