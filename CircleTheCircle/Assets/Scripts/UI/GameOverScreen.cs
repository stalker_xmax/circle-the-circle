﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScreen : ScreenBase
{

    [SerializeField]
    private Text _txtScore;

    public override void Show()
    {
        base.Show();
    }

    public override void Hide()
    {
        base.Hide();
    }

    public override void UpdateData(object param)
    {
        base.UpdateData(param);
        UIManager.SetText(_txtScore, param.ToString());
    }

    public void OnBackTitle()
    {
        UIManager.Instance.ChangeState(UIState.Title);
    }
}
