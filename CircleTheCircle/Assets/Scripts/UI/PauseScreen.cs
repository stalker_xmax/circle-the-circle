﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseScreen : ScreenBase
{
    public override void Show()
    {
        base.Show();
    }

    public override void Hide()
    {
        base.Hide();
    }

    public void OnContinue()
    {
        UIManager.Instance.ChangeState(UIState.Continue);
    }

    public void OnExit()
    {
        Application.Quit();
    }
}
