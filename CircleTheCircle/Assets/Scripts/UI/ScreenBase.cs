﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ScreenBase : MonoBehaviour
{
    public virtual void Show()
    {
        if (gameObject != null)
        {
            gameObject.SetActive(true);
        }
        
    }

    public virtual void Hide()
    {
        if (gameObject != null)
        {
            gameObject.SetActive(false);
        }
    }
    public virtual void UpdateData(object param)
    {

    }
}
