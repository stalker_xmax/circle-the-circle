﻿
using UnityEngine;
using UnityEngine.UI;

public enum UIState
{
    Title,
    Play,
    Continue,
    Pause,
    GameOver
}

public class UIManager : AutoCreateInstance<UIManager>
{
    [SerializeField]
    private ScreenBase _titleScreen;
    [SerializeField]
    private ScreenBase _playScreen;
    [SerializeField]
    private ScreenBase _pauseScreen;
    [SerializeField]
    private ScreenBase _gameOverScreen;

    public void ChangeState(UIState state)
    {
        State = state;
        Time.timeScale = 1;
        Debug.Log("UI Change state :" + state);
        switch (state)
        {
            case UIState.Title:
                _titleScreen.Show();
                _playScreen.Hide();
                _pauseScreen.Hide();
                _gameOverScreen.Hide();
                GameManager.Instance.Replay();
                break;
            case UIState.Play:
                _titleScreen.Hide();
                _playScreen.Show();
                _pauseScreen.Hide();
                _gameOverScreen.Hide();
                GameManager.Instance.StartGame();
                break;
            case UIState.Continue:
                _titleScreen.Hide();
                _playScreen.Show();
                _pauseScreen.Hide();
                _gameOverScreen.Hide();
                State = UIState.Play;
                break;
            case UIState.Pause:
                _titleScreen.Hide();
                _playScreen.Show();
                _pauseScreen.Show();
                _gameOverScreen.Hide();
                Time.timeScale = 0;
                break;
            case UIState.GameOver:
                _titleScreen.Hide();
                _playScreen.Hide();
                _pauseScreen.Hide();
                _gameOverScreen.Show();
                _gameOverScreen.UpdateData(GameManager.Instance.Score);
                break;
        }
    }

    public UIState State;

    private void Start()
    {
        ChangeState(UIState.Title);
    }

    public void UpdateScore(int score)
    {
        _playScreen.UpdateData(score);
    }

    public static void SetText(Text txt, string value)
    {
        if (txt != null)
        {
            txt.text = value;
        }
    }

    public static void SetActive(GameObject go, bool active)
    {
        if (go != null && go.activeSelf)
        {
            go.SetActive(active);
        }
    }
}
