﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayScreen : ScreenBase
{
    [SerializeField]
    private Text _txtScore;

    public override void Show()
    {
        base.Show();
    }

    public override void Hide()
    {
        base.Hide();
    }

    public void OnPause()
    {
        UIManager.Instance.ChangeState(UIState.Pause);
    }

    public override void UpdateData(object param)
    {
        base.UpdateData(param);
        UIManager.SetText(_txtScore, param.ToString());
    }
}
