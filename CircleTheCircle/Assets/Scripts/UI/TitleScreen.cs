﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleScreen : ScreenBase
{
    public override void Show()
    {
        base.Show();
    }

    public override void Hide()
    {
        base.Hide();
    }

    public void OnStart()
    {
        UIManager.Instance.ChangeState(UIState.Play);
    }

    public void OnExit()
    {
        Application.Quit();
    }
}
