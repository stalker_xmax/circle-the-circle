﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public float PositionOfY;

    private bool _isAlive;

    void OnEnable()
    {
        _isAlive = true;
        transform.position = new Vector2(transform.position.x, PositionOfY);
    }

    void OnDisable()
    {
        _isAlive = false;
    }

    void Update()
    {
        if (!_isAlive)
        {
            return;
        }

        if (GameManager.Instance.State == GameState.Play)
            transform.Translate(Vector3.left * Mathf.Ceil(GameManager.Instance.GameSpeed) * Time.smoothDeltaTime, Space.World);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "OutScreen")
        {
            ObjectPool.Instance.Destroy(gameObject);
        }
    }
}
